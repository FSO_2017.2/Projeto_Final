# Projeto Final utilizando Device Drivers e worms feito na disciplina de Fundamentos de Sistemas Operacionais

## Integrantes
* 14/0080619 Camila Carneiro Ferrer Santos
* 14/0066543 Vinicius Pinheiro da Silva Correa

## Execução da aplicação

### Pré requisitos
Será necessário instalar `linux-headers-..` como mostrado abaixo.

```
sudo apt-get install build-essential
```

### Compilação
Para compilar o código execute o Makefile:
```
make
```

### Execução

#### Instalando o device driver
Para instalar o módulo kernel execute o comando:
```
sudo insmod <file>.ko
```

#### Removendo o device driver
Para desinstalar o módulo kernel execute o comando:
```
sudo rmmod <file>.ko
```

#### Instalando o device driver
Para exibir as menssagens de instalação e remoção execute:
```
dmesg | tail -1
```

