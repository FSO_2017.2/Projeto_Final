#!/bin/bash
MODULE=LEDsWithKeylogger
MAJOR=60
DEV=keylogger
MODE=700

make

sudo insmod "$MODULE".ko || exit 1

sudo mknod /dev/$DEV c $MAJOR 0
